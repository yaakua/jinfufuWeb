// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App.vue'
import { WechatPlugin, AjaxPlugin } from 'vux'
// 自定义的路由文件
import router from './router/index.js'
// 自定义的公共函数和公共请求方法
import Base from './assets/js/baseFun.js'
// 自定义的全局变量
import stores from './store/store.js'
// 引入的全局公共css
import './assets/css/base.css'

Vue.use(VueRouter)
Vue.use(WechatPlugin)
Vue.use(AjaxPlugin)

FastClick.attach(document.body)

Vue.config.productionTip = false

// 注册全局函数和全局常量
// 注册到vue的全局，方便各个组件和页面js调用公共函数
Vue.prototype.baseFun = Base.baseFun
// 将封装的ajax请求函数注册到vue的全局
Vue.prototype.baseAjax = Base.baseAjax
// require('../src/service/mock')
/* eslint-disable no-new */
new Vue({
  router,
  store: stores,
  render: h => h(App)
}).$mount('#app-box')
