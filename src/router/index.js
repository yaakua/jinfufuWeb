import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import SecondPage from '../components/SecondPage.vue'

import HomePage from '../components/homePage/HomePage.vue'
import IncreaseCredit from '../components/homePage/IncreaseCredit.vue'
import StartLoan from '../components/homePage/StartLoan.vue'
import Partner from '../components/homePage/Partner.vue'
import FinancialTool from '../components/homePage/FinancialTool.vue'
import PartnerSystem from '../components/homePage/PartnerSystem.vue'
import ContactUs from '../components/homePage/ContactUs.vue'

import FinancialMatter from '../components/financialMatter/FinancialMatter.vue'
import FinancialConsulting from '../components/financialConsulting/FinancialConsulting.vue'

import My from '../components/my/My.vue'
import Share from '../components/my/Share.vue'
import StorePage from '../components/common/StorePage.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/jinFuFu',
      redirect: '/jinFuFu/homePage',
      component: Home,
      children: [{
        path: 'homePage',
        name: 'HomePage',
        component: HomePage
      }, {
        path: 'matter',
        name: 'FinancialMatter',
        component: FinancialMatter
      }, {
        path: 'consulting',
        name: 'FinancialConsulting',
        component: FinancialConsulting
      }, {
        path: 'my',
        name: 'My',
        component: My
      }]
    },
    {
      path: '/jinFuFu/secondPage',
      component: SecondPage,
      children: [{
        path: 'increaseCredit',
        name: 'IncreaseCredit',
        component: IncreaseCredit
      }, {
        path: 'startLoan',
        name: 'StartLoan',
        component: StartLoan
      }, {
        path: 'partner',
        name: 'Partner',
        component: Partner
      }, {
        path: 'financialTool',
        name: 'FinancialTool',
        component: FinancialTool
      }, {
        path: 'partnerSystem',
        name: 'PartnerSystem',
        component: PartnerSystem
      }, {
        path: 'contactUs',
        name: 'ContactUs',
        component: ContactUs
      }, {
        path: 'share',
        name: 'Share',
        component: Share
      }, {
        path: 'storePage',
        name: 'StorePage',
        component: StorePage
      }]
    }
  ]
})
